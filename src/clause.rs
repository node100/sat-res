use std::fmt::{Display, Debug};

#[derive(Clone, PartialEq, Eq, Copy)]
pub enum Lit {
    P(usize),
    N(usize)
}

impl Lit {
    pub fn id(&self) -> usize {
        match self {
            Lit::P(n) => *n,
            Lit::N(n) => *n
        }
    }

    fn is_neg(&self) -> bool {
        match self {
            Lit::P(_) => false,
            _ => true
        }
    }

    // fn is_pos(&self) -> bool {
    //     !self.is_neg()
    // }

    fn is_dual(&self, other: &Lit) -> bool {
        match self {
            Lit::P(n) if *other == Lit::N(*n) => true,
            Lit::N(n) if *other == Lit::P(*n) => true,
            _ => false
        }
    }

    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        if self.id() == other.id() {
            if self.is_neg() == other.is_neg() {
                std::cmp::Ordering::Equal
            } else if self.is_neg() {
                std::cmp::Ordering::Greater
            } else {
                std::cmp::Ordering::Less
            }            
        } else {
            self.id().cmp(&other.id())
        }
    }
}

impl PartialOrd for Lit {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Lit {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.cmp(other)
    }
}

impl Display for Lit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Lit::P(n) => write!(f, "{}", n),
            Lit::N(n) => write!(f, "¬{}", n)
        }
        
    }
}

impl Debug for Lit {
     fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Lit::P(n) => write!(f, "{}", n),
            Lit::N(n) => write!(f, "¬{}", n)
        }
        
    }
}


#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Clause {
    // invariant: sorted and deduplicated
    data: Vec<Lit>,
    //todo: store reference to parent clauses, to provide unsat proof
    //parents: (Rc<Clause>, Rc<Clause>)
}

impl Display for Clause {

    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;
        let mut itr = self.literals().iter();
        if let Some(l) = itr.next() {
            write!(f, "{}", l)?;
        }
        for l in itr {
            write!(f, ", {}", l )?;
        }
        write!(f, "]")
    }
}

impl Clause {
// we need unique representation of clauses in order to compare new resolvents to already known clauses
    pub fn from(mut literals: Vec<Lit>) -> Self {
        literals.sort();
        literals.dedup();
        Clause {
            data: literals
        }
    }

    pub fn literals(&self) -> &Vec<Lit> {
        &self.data
    }

    pub fn is_empty(&self) -> bool {
    	self.data.is_empty()
    }

    pub fn resolve(&self, other: &Clause) -> Self {
        let mut slf_itr = self.data.iter();
        let mut oth_itr = other.data.iter();
        let mut result: Vec<Lit> = vec!();

        if let Some(mut slf_lit) = slf_itr.next() {
            if let Some(mut oth_lit) = oth_itr.next() {

                loop {
                    if slf_lit.is_dual(oth_lit) {            
                        match slf_itr.next() {
                            Some(l) => slf_lit = l,
                            _ => break
                        }
                        match oth_itr.next() {
                            Some(l) => oth_lit = l,
                            _ => {
                                result.push(*slf_lit);
                                break
                            }
                        }
                    } else if slf_lit == oth_lit {
                        result.push(*slf_lit);

                        match slf_itr.next() {
                            Some(l) => slf_lit = l,
                            _ => break
                        }
                        match oth_itr.next() {
                            Some(l) => oth_lit = l,
                            _ => {
                                result.push(*slf_lit);
                                break
                            }
                        }
                    } else if slf_lit < oth_lit {
                        result.push(*slf_lit);
                        
                        match slf_itr.next() {
                            Some(l) => slf_lit = l,
                            _ => {
                                result.push(*oth_lit);
                                break
                            }
                        }
                    } else {
                        result.push(*oth_lit);
                        match oth_itr.next() {
                            Some(l) => oth_lit = l,
                            _ => {
                                result.push(*slf_lit);
                                break
                            }
                        }
                    }
                }
            }
        }

        for l in slf_itr.chain(oth_itr) {
            result.push(*l)
        }

        println!("resolved {:?} and {:?} to {:?}", self.data, other.data, result);

        Clause::from(result)
    }
}
