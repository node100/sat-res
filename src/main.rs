use std::rc::Rc;
use std::collections::{BTreeSet};

mod clause;
use clause::{Lit, Clause};

fn main() {
    println!("Hello, world!");

    use Lit::{P, N};

    let input: Vec<Clause> = vec!(
        Clause::from(vec!(P(0), N(1), P(2))),
        Clause::from(vec!(N(0), N(1), P(3)))
    );    
    println!("{:?}", solve(input));

}


fn solve(mut input_q: Vec<Clause>) -> Option<Vec<Lit>> {

    let max_lit: usize = input_q.iter().map(|c|c.literals()).flatten().fold(0, |max, lit| {max.max(lit.id())});
    let mut cl: Vec<(Vec<Rc<Clause>>, Vec<Rc<Clause>>)> = vec![(vec!(), vec!()); max_lit+1];

    let mut closure: BTreeSet<Rc<Clause>> = BTreeSet::new();

    while let Some(clause) = input_q.pop() {
        // the clause list is mutable (growing) while references to its elements exist in the per-literal lists (`cl`)
        let clause = Rc::new(clause);
        let c = Rc::clone(&clause);
        
        if closure.insert(clause) {
            for lit in c.literals().iter() {
                match lit {
                    Lit::P(id) => {
                        cl[*id].0.push(Rc::clone(&c));
                        for c2 in cl[*id].1.iter() {
                            let r = c.resolve(c2);
                            if r.is_empty() {
                                return None;
                            } else {
                                input_q.push(r);
                            }
                        }
                    },
                    Lit::N(id) => {
                        cl[*id].1.push(Rc::clone(&c));
                        for c2 in cl[*id].0.iter() {
                            let r = c.resolve(c2);
                            if r.is_empty() {
                                return None;
                            } else {
                                input_q.push(r);
                            }
                        }
                    },
                }
            }
        }
    }

    Some(solution(closure))
}

fn solution(c: BTreeSet<Rc<Clause>>) -> Vec<Lit> {


    todo!("sat, solution to be computed")
}



